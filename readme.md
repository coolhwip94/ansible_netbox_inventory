# Ansible Netbox Inventory
> This repo serves multiple purposes
> - Showcase an example of using Netbox for dynamic inventory
> - Showcase running commands on network devices utilizing ansible, and ansible-galaxy collections <br>
> ** Note this repo will have some lab device credentials, exposed for the purpose of verbosity, in production environments ansible vault should be used to protect passwords, and proprietary information.

# NOTE :
- proprietary data has been scrubbed (dev credentials remain, I made those for practice so they dont matter)
- This is to be referenced as examples rather than a working repo

## Setup
---
1. Setup python environment
    ```
    # Virtual Env
    python3 -m venv venv

    # Activate Env
    source venv/bin/activate

    # Install packages
    pip3 install -r requirements.txt
    ```

## Ansible Playbooks
---
- `cisco.yml` : Playbook to run tasks against cisco ios host setup in lab
    - running this playbook:
    > note: This will use variables from netbox
    ```
    ansible-playbook cisco.yml
    ```

## Dynamic Inventory Using Netbox
---
- `netbox_inventory.yml`
  - this utilizes the netbox inventory plugin in ansible
  
  <br>
- Viewing Inventory
    > This will output the inventory in json format for viewing
    ```
    ansible-inventory --list

    # In an environment where inventory isnt set in `ansible.cfg`
    ansible-inventory -i netbox_inventory.yml --list
    ```
    > Example Output in `example_netbox_inventory.json`
    
  <br>
- Using Inventory
    > Using the `netbox_inventory.yml` is similar to using normal host file
    ```
    # View inventory
    ansible-inventory --list
    # or
    ansible-inventory -i netbox_inventory.yml --list

    # Using in playbook
    ansible-inventory -i netbox_inventory.yml cisco.yml
    ```


## References
- https://docs.ansible.com/ansible/latest/collections/cisco/ios/index.html#plugins-in-cisco-ios
- https://gitlab.presidio.com/pzharyuk/crowcastle_ansible_netbox/-/tree/main