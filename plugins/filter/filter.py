class FilterModule:

    @staticmethod
    def filters():
        '''
        Filters will be called from here. Call from ansible playbook similar to a jinja filter

        Ex : " {{ variable | filter }} "

        First ARG is value that is before the | symbol, additional values can be passed

        "{{ ARG1 | filter(ARG2,ARG3) }}"
        '''

        return {
            'convert_cisco_int' : FilterModule.convert_cisco_int
        }


    @staticmethod
    def convert_cisco_int(interface):
        '''
        A simple example of a customer filter
        '''
        # this can simply be changed in netbox, 
        # but purpose is to demonstrate a translation if needed
        # customizable with python
        return interface.replace("ge-","GigabitEthernet")
